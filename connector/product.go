package connector

import (
	"encoding/json"
	"fmt"
	"white-tea-go/models"
	"white-tea-go/utils"
)

func (conn Connector) GetProducts() models.Product {
	products := models.Product{}
	mdata := conn.DataRepository.FirebaseRepository.FindAll()
	utils.MapToStruct(mdata, products)
	fmt.Println("mdata: ", mdata)

	strdata, err := json.Marshal(mdata)
	if err != nil {
		fmt.Println(err.Error())
	}

	json.Unmarshal([]byte(strdata), &products)

	fmt.Println(";;;;;", strdata)
	return products
}
