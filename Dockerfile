FROM golang:alpine
ADD . /go/src/xmlparser
RUN go install xmlparser
CMD ["/go/bin/xmlparser"]
EXPOSE 3000