package database

import (
	"context"
	"fmt"
	"log"
	"white-tea-go/models"
	"white-tea-go/utils"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/db"
	"google.golang.org/api/option"
)

var client *db.Client

type FirebaseRepository struct {
	CredentialsFilePath string
	Database            string
	Collection          string
}

func (r FirebaseRepository) initial() firestore.Client {
	opt := option.WithCredentialsFile(r.CredentialsFilePath)

	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v", err)
	}

	client, err := app.Firestore(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	return *client
}

//FindAll : function to find data from firebase db
func (r FirebaseRepository) FindAll() map[string]interface{} {
	client := r.initial()

	documentRef := client.Collection(r.Database).Doc(r.Collection)

	dsnap, err := documentRef.Get(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(">>>>>", dsnap.Data())
	defer client.Close()
	return dsnap.Data()
}

//Set : function to create or update existing data
func (r FirebaseRepository) Set() {
	client := r.initial()
	product := models.Product{
		ID:    utils.GenerateUUIDWithPrefix("product"),
		CatID: "cat:123456",
		Title: models.Title{
			Thai:    "thai",
			English: "test",
		},
	}

	documentRef := client.Collection(r.Database).Doc(r.Collection)
	_, documentRefErr := documentRef.Set(context.Background(), product)
	if documentRefErr != nil {
		fmt.Println(documentRefErr)
	}
	defer client.Close()
}
