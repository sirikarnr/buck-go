package repository

import "white-tea-go/repository/database"

//DataRepository : database repository interface
type DatabaseRepository interface {
	Initial() interface{}
	FindAll()
	Create()
	Set()
	Update()
}

type DataReposirory struct {
	FirebaseRepository database.FirebaseRepository
}
