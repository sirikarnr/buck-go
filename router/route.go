package router

import (
	"net/http"
	"white-tea-go/connector"
	"white-tea-go/handler"

	"github.com/gorilla/mux"
)

const (
	HttpMethodGet = "GET"
)

type Router struct {
	ProductHandler handler.ProductHandler
}

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter(connector connector.Connector) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	var routes = Routes{
		AddRoute("highlights", HttpMethodGet, "/highlights", handler.GetHighlights),
		AddRoute("products", HttpMethodGet, "/products", handler.NewHandler(connector).GetProducts),
	}

	for _, route := range routes {
		router.Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

func AddRoute(name, method, url string, handleFunc http.HandlerFunc) Route {
	return Route{
		Name:        name,
		Method:      method,
		Pattern:     url,
		HandlerFunc: handleFunc,
	}
}
