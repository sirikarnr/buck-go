package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"white-tea-go/constant"
	"white-tea-go/models"
	"white-tea-go/response"
	"white-tea-go/utils"
)

//GetHighlights : get highlight data
func GetHighlights(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html;charset=UTF-8")

	data, err := http.Get(constant.GetHighlightURL)
	bodyBytes, err := ioutil.ReadAll(data.Body)
	if err != nil {
		log.Fatal(err)
	}
	bodyString := string(bodyBytes)

	highlights := []models.Highlight{}

	strres := utils.ConvertToModel(bodyString, highlights, "highlights")

	err = json.Unmarshal([]byte(strres), &highlights)
	if err != nil {
		fmt.Println(err)
	}

	resp := response.SuccessResponse{
		Data: highlights,
		Code: http.StatusOK,
	}
	fmt.Fprintf(w, utils.ToResponse(resp))
}
