package handler

import (
	"fmt"
	"net/http"
	"white-tea-go/connector"
	"white-tea-go/response"
	"white-tea-go/utils"
)

type ProductHandler interface {
	GetProducts(w http.ResponseWriter, r *http.Request)
}

type ProductHandlerImpl struct {
	Connector connector.Connector
}

func NewHandler(connector connector.Connector) ProductHandler {
	productHandlerImpl := ProductHandlerImpl{Connector: connector}
	return productHandlerImpl
}

//GetProducts : get product data
func (h ProductHandlerImpl) GetProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html;charset=UTF-8")
	products := h.Connector.GetProducts()
	resp := response.SuccessResponse{
		Data: products,
		Code: http.StatusOK,
	}
	fmt.Fprintf(w, utils.ToResponse(resp))
}
