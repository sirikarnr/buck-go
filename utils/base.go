package utils

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"xmlparser/models"

	"github.com/google/uuid"
)

//ParseJSONToXML : parse json to xml
func ParseJSONToXML() string {
	var xmlResp models.MerchantResponseData

	jsonStr := `{"ErrorCode":"test","CustomerID":"ABC1234"}`
	json.Unmarshal([]byte(jsonStr), &xmlResp)
	out, _ := xml.MarshalIndent(xmlResp, "\t", "\t")

	return string(out)
}

func ToResponse(content interface{}) string {
	b, _ := json.Marshal(content)
	return string(b)
}

func ConvertToModel(data string, model interface{}, sourceField string) string {
	var mdata []map[string]interface{}
	err := json.Unmarshal([]byte(data), &mdata)
	if err != nil {
		fmt.Println(err)
	}
	strdata, err := json.Marshal(mdata[0][sourceField])
	if err != nil {
		fmt.Println(err.Error())
	}

	return string(strdata)
}

func GenerateUUIDWithPrefix(prefix string) string {
	return fmt.Sprintf("%v:%v", prefix, uuid.New())
}
