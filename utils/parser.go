package utils

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"xmlparser/models"
)

//ParserMerchantResponse : MerchantResponse parser
func ParserMerchantResponse(file *os.File) (models.MerchantResponseData, error) {
	var mc models.MerchantResponseData
	if err := xml.NewDecoder(file).Decode(&mc); err != nil {
		return models.MerchantResponseData{}, err
	}
	defer file.Close()

	return mc, nil
}

//MapToStruct : function to convert map to struct
func MapToStruct(mdata map[string]interface{}, model interface{}) {
	strdata, err := json.Marshal(mdata)
	if err != nil {
		fmt.Println(err.Error())
	}

	json.Unmarshal([]byte(strdata), &model)
	fmt.Println("******model*******", model)
}
