package utils

import (
	"fmt"
	"strings"
	"testing"
)

func TestReadXMLSuccess(t *testing.T) {
	resp, _ := ReadXML("../test/assets/test.xml")
	if resp == nil {
		t.Error("TestReadXML should return response")
	}
}

func TestReadXMLFail(t *testing.T) {
	_, err := ReadXML("../test/assets/test")
	expect := "The system cannot find the file specified."
	if !strings.ContainsAny(expect, err.Error()) {
		t.Error("TestReadXML should return error")
	}
}

func TestReadFolderSuccess(t *testing.T) {
	files, _ := ReadFolder("../test/assets/")
	expect := 2
	if len(files) != expect {
		t.Error(fmt.Sprintf("TestReadFolderSuccess should return %v got %v", expect, len(files)))
	}
}

func TestReadFolderFail(t *testing.T) {
	_, err := ReadFolder("")
	fmt.Println(err)
	expect := "The system cannot find the file specified."
	if !strings.ContainsAny(expect, err.Error()) {
		t.Error("TestReadXML should return error")
	}
}

func BenchmarkTestReadXML(b *testing.B) {
	// run function b.N times
	for n := 0; n < b.N; n++ {
		ReadXML("../test/assets/test.xml")
	}
}

func BenchmarkTestReadFolder(b *testing.B) {
	// run function b.N times
	for n := 0; n < b.N; n++ {
		ReadFolder("../test/assets/test.xml")
	}
}
