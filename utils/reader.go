package utils

import (
	"io/ioutil"
	"os"
	"path/filepath"
)

//ReadXML : function for read xml file
func ReadXML(path string) (*os.File, error) {
	filePath, _ := filepath.Abs(path)

	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	return file, nil
}

//ReadFolder : function for list file in directory
func ReadFolder(dir string) ([]os.FileInfo, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return []os.FileInfo{}, err
	}

	return files, nil
}
