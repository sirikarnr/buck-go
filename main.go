package main

import (
	"log"
	"net/http"
	"white-tea-go/connector"
	"white-tea-go/constant"
	"white-tea-go/repository"
	db "white-tea-go/repository/database"
	"white-tea-go/router"
	"xmlparser/utils"
)

func main() {
	utils.ParseJSONToXML()

	firebaseRepo := db.FirebaseRepository{
		CredentialsFilePath: constant.CredentialFilePath,
		Database:            constant.Database,
		Collection:          constant.Collection}

	var dataRepo = repository.DataReposirory{FirebaseRepository: firebaseRepo}
	connector := connector.Connector{
		DataRepository: dataRepo,
	}
	router := router.NewRouter(connector)

	log.Fatal(http.ListenAndServe(":3000", router))

}
