package models

type Title struct {
	Thai    string `json:"th"`
	English string `json:"en"`
}

type ProductImages struct {
	CoverImg string   `json:"cover"`
	ImgsURL  []string `json:"imgs_url"`
}
type Product struct {
	ID          string        `json:"id"`
	CatID       string        `json:"cat_id"`
	Title       Title         `json:"title"`
	Description string        `json:"description"`
	Quantity    int32         `json:"quantity"`
	Images      ProductImages `json:"images"`
}
