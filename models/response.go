package models

//SuccessResponse : model for success response
type SuccessResponse struct {
	Data string `json:"result"`
	Code *int   `json:"code,omitempty"`
}
