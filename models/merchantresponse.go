package models

import "encoding/xml"

//MerchantResponse : model of entire document
type MerchantResponse struct {
	XMLName xml.Name `xml:"MerchantResponse"`
}

//MerchantResponseData : model of child node
type MerchantResponseData struct {
	ErrorCode       string  `xml:"MerchantResponse>ErrorCode" json:"ErrorCode"`
	CustomerID      string  `xml:"MerchantResponse>CustomerID" json:"CustomerID"`
	AuthToken       string  `xml:"MerchantResponse>AuthToken" json:"AuthToken"`
	Balance         float64 `xml:"MerchantResponse>Balance" json:"Balance"`
	OpenBetsBalance string  `xml:"MerchantResponse>OpenBetsBalance" json:"OpenBetsBalance"`
	TransactionID   string  `xml:"MerchantResponseTransactionID" json:"TransactionID"`
}
