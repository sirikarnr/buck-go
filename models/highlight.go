package models

type Highlight struct {
	BgImage     string `json:"bgImage"`
	Buttone     Button `json:"button"`
	Description string `json:"description"`
	Title       string `json:"title"`
}

type Button struct {
	ButtonTitle string `json:"buttonTitle"`
	InternalURL string `json:"internalUrl"`
}
