package constant

const (
	GetHighlightURL = "https://jatuphon-web.firebaseio.com/home.json"
	ProjectID       = "littlebbear-abe46"

	Database           = "littlebbear"
	Collection         = "products"
	CredentialFilePath = `repository\database\assets\littlebbear-abe46-firebase-adminsdk-80lkp-2e9a150701.json`
)
